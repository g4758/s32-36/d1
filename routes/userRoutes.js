const express = require('express');
const router = express.Router();

// Import units of functions from userController module
const {
	register,
	getAllUsers,
	checkEmail,
	login,
	profile
	
} = require('./../controllers/userControllers');

const {verify, decode} = require('./../auth');

//GET ALL USERS
router.get('/', async (req, res) => {

	try{
		await getAllUsers().then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})


//REGISTER A USER 
router.post('/register', async (req, res) => {
	// console.log(req.body)	//user object

	try{
		await register(req.body).then(result => res.send(result))

	} catch(err){
		res.status(500).json(err)
	}
})


//CHECK IF EMAIL ALREADY EXISTS
router.post('/email-exists', async (req, res) => {
	try{
		await checkEmail(req.body).then(result => res.send(result))

	}catch(error){
		res.status(500).json(error)
	}
})



//LOGIN THE USER
	// authentication
router.post('/login', (req, res) => {
	// console.log(req.body)
	try{
		login(req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})



//RETRIEVE USER INFORMATION
router.get('/profile', verify, async (req, res) => {
	// console.log(req.headers.authorization)
	const userId = decode(req.headers.authorization).id
	// console.log(userId)

	try{
		profile(userId).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})


//Create a route to update user information, make sure the route is secured with token


//Create a route to update user's password, make sure the  route is secured with token


//Create a route to dekete a user from the DB, make sure the route is secured
	// Stretch goal, only admin can delete a user



//Export the router module to be used in index.js file
module.exports = router;
